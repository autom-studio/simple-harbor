# 在本地快速部署 harbor

## 环境准备

准备一个安装有 docker 和 docker-compose 的主机即可。

## 下载离线安装包

下载最新版本 harbor 离线安装包，当前版本： v2.4.0

下载地址： https://cowtransfer.com/s/46bd054088a344

## 解压并修改配置文件

只需修改 harbor.yml ，修改项如下：

```
hostname: hub.atompi.cc    # 访问域名或IP地址
# 如果没有ssl证书，则注释掉 https 字段
harbor_admin_password: admin123    # 初始管理员密码
data_volume: /data    # 镜像文件存储路径，需要指定一个空文件夹且空间足够
```

## 执行安装命令

```
# 同时开启 helm chart 仓库支持
sudo ./install.sh --with-chartmuseum
```

## 完成
